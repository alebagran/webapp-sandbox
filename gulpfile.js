/*jshint node: true */
'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var cssnano = require('gulp-cssnano');
var uglify = require('gulp-uglify');
var del = require('del');
var nodemon = require('gulp-nodemon');
var sync = require('browser-sync').create();

// Minify CSS
gulp.task('min-css', function() {
  return gulp.src('src/css/*.css')
    .pipe(concat('styles.min.css'))
    .pipe(cssnano())
    .pipe(gulp.dest('app/public/css'));
});

// Minify JS
gulp.task('min-js', function() {
  return gulp.src('src/js/*.js')
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/public/js'));
});

// Clean compiled files and repo
gulp.task('clean', function() {
  return del.sync([
    'app/public/css/',
    'app/public/js/'
  ]);
});

// Build
gulp.task('build', ['clean', 'min-css', 'min-js']);

// Daemon
gulp.task('nodemon', function() {
  return nodemon({
    script: 'app/app.js'
  });
});

// Browser sync
gulp.task('serve', ['nodemon'], function() {
  sync.init(null, {
    proxy: 'http://localhost:1337',
    browser: 'firefox',
    files: 'app/**/*',
    port: 3000,
    ghostMode: false
  });

  gulp.watch('src/css/*.css', ['min-css']);
  gulp.watch('src/js/*.js', ['min-js']);
});
