/*jshint node: true */

'use strict';

// Main vars
var express = require('express');
var path = require('path');

// App instance
var app = express();

// Static path
app.use('/public', express.static(path.join(__dirname, 'public')));

// Router init
app.use(express.Router());

// "Landing" page
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

// Boot it up
app.listen(1337, '127.0.0.1');
