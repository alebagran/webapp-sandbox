# webapp-sandbox
node.js/express.js app starter with live reload ready out of the box.

# instalation
`npm install`

# building
`gulp build`

# editing and live reloading
`gulp serve`

Always (re)build when live reload starts acting funny. Could be a bad auto save that put the process in trouble.